class FeedPostView {
  final String title;
  final String description;
  final String creatorName;
  final String createdOnDate;
  final String avatarPictureUrl;
  final List<String> files;
  final Map<String, String> stores;
  final int stars;
  final bool starredByUser;

  FeedPostView({this.stores, this.title, this.description, this.creatorName, this.createdOnDate, this.avatarPictureUrl, this.files, this.stars, this.starredByUser});
}
