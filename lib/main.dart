import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fribbble/bloc/login_bloc.dart';
import 'package:fribbble/bloc_provider.dart';
import 'package:fribbble/fribbble_context.dart';
import 'package:fribbble/gateway/fribbble_gateway.dart';
import 'package:fribbble/pages/feed_page.dart';
import 'package:fribbble/pages/login_page.dart';

import 'FribbleTheme.dart';

void main() => runApp(Fribbble());

class Fribbble extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.signInAnonymously();
    return StreamBuilder<FirebaseUser>(
      stream: FirebaseAuth.instance.onAuthStateChanged,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return FribbbleApp(fbUser: snapshot.data);
        }
        return AuthApp();
      },
    );
  }
}

class FribbbleApp extends StatelessWidget {
  const FribbbleApp({Key key, @required this.fbUser}) : super(key: key);

  final FirebaseUser fbUser;

  @override
  Widget build(BuildContext context) {
    final gateway = FribbbleGateway(fbUser);
    return BlocProvider(
      bloc: FribbbleContext(gateway),
      child: FribbbleMaterialApp(
        home: FeedPage(),
        routes: {
          FeedPage.tag: (context) => FeedPage(),
        },
      ),
    );
  }
}

class AuthApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginBloc>(
      bloc: LoginBloc(),
      child: FribbbleMaterialApp(
        home: LoginPage(),
        routes: {
          LoginPage.tag: (context) => LoginPage(),
        },
      ),
    );
  }
}

class FribbbleMaterialApp extends StatelessWidget {
  const FribbbleMaterialApp(
      {Key key, this.home, this.onUnknownWidget, this.routes})
      : super(key: key);

  final Widget home;
  final Widget onUnknownWidget;
  final Map<String, WidgetBuilder> routes;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fribbble',
      home: home,
      theme: FribbbleThemeData,
      routes: routes,
      onUnknownRoute: (_) =>
          MaterialPageRoute(builder: (context) => onUnknownWidget),
    );
  }
}