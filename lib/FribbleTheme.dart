import 'package:flutter/material.dart';

final ThemeData FribbbleThemeData = new ThemeData(
  brightness: Brightness.light,
  primaryColor: Color(0xFF279CFC),
  accentColor: Color(0xFF6630FF),
  bottomAppBarColor: Color(0xFF093D6D),
  scaffoldBackgroundColor: Color(0xFFFFFFFF),
  cardColor: Color(0xFFFFFFFF),
  textTheme: TextTheme(
    display1: TextStyle(
      fontFamily: 'Raleway',
      fontSize: 32,
      color: Color(0xFF6630FF),
      fontWeight: FontWeight.bold,
    ),
    display2: TextStyle(
      fontFamily: 'RobotoCondensed',
      fontSize: 20,
      color: Color(0xFF279CFC),
      fontWeight: FontWeight.normal,
    ),
    subtitle: TextStyle(
      fontFamily: 'RobotoCondensed',
      fontSize: 12,
      color: Color(0xFF279CFC),
      fontWeight: FontWeight.bold,
      letterSpacing: 2,
    ),
    body1: TextStyle(
      fontFamily: 'Raleway',
      fontSize: 16,
      color: Color(0xFF505B66),
      fontWeight: FontWeight.normal,
    ),
    body2: TextStyle(
      fontFamily: 'Raleway',
      fontSize: 10,
      color: Color(0xFF788999),
      fontWeight: FontWeight.normal,
    ),
    button: TextStyle(
      fontFamily: 'RobotoCondensed',
      fontSize: 14,
      color: Color(0xFF788999),
      fontWeight: FontWeight.bold,
    ),
  )
);