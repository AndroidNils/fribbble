import 'package:flutter/material.dart';
import 'package:fribbble/bloc/project_add_bloc.dart';
import 'package:fribbble/bloc_provider.dart';
import 'package:fribbble/pages/project_add/next_button.dart';

class ProjectAddDescriptionPage extends StatelessWidget {
  const ProjectAddDescriptionPage({Key key, @required this.bloc})
      : super(key: key);

  final ProjectAddBloc bloc;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: bloc,
      child: _ProjectAddTitlePage(),
    );
  }
}

class _ProjectAddTitlePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _DescriptionField(),
            FinishButton(),
          ],
        ),
      ),
    );
  }
}

class _DescriptionField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<ProjectAddBloc>(context);
    return TextField(
      decoration: InputDecoration(labelText: 'Description'),
      onChanged: bloc.changeDescription,
    );
  }
}
