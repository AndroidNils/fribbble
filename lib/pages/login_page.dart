import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: [
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

class LoginPage extends StatelessWidget {
  static const String tag = 'login-page';

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              // A fixed-height child.
              color: const Color(0xff808000), // Yellow
              height: 120.0,
            ),
            RaisedButton(
              child: const Text("Login"),
              onPressed: _handleSignIn,
            )
          ],
        ),
      ),
    );
  }
}
