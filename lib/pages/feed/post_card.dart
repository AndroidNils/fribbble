import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fribbble/pages/post_detail_page.dart';
import 'package:fribbble/views/feed_post_view.dart';

import '../../FribbleTheme.dart';

class PostCard extends StatelessWidget {
  final FeedPostView post;

  const PostCard({Key key, this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.push(context,
          MaterialPageRoute(builder: (context) => PostDetailPage(post))),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16,4,16,4),
        child: Card(
          elevation: 8,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: Column(
            children: <Widget>[
              _Header(
                date: post.createdOnDate,
                name: post.creatorName,
                avatarUrl: post.avatarPictureUrl,
              ),
              _ProjectPicture(
                downloadUrl: post.files[0],
              ),
              _ProjectMeta(
                description: post.description,
                likes: post.stars,
                title: post.title,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _ProjectMeta extends StatelessWidget {
  const _ProjectMeta({Key key, this.title, this.description, this.likes})
      : super(key: key);

  final String title;
  final String description;
  final int likes;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(24,16,24,0),
          child: Text(title,
              style: FribbbleThemeData.textTheme.display1
                  .copyWith(color: Theme.of(context).bottomAppBarColor)),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(24,6,24, 0),
          child: Text(description, style: FribbbleThemeData.textTheme.body1),
        ),
        InkWell(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(0,16,16,16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Icon(Icons.star, color: Color(0xFFF7C203)),
                Padding(
                  padding: const EdgeInsets.fromLTRB(4, 0, 0, 0),
                  child: Text(likes.toString(),
                      style: FribbbleThemeData.textTheme.body1
                          .copyWith(color: Color(0xFFF7C203))),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _ProjectPicture extends StatelessWidget {
  const _ProjectPicture({Key key, this.downloadUrl}) : super(key: key);

  final String downloadUrl;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: 150,
      child: CachedNetworkImage(
        imageUrl: downloadUrl,
        placeholder: (context, url) => new CircularProgressIndicator(),
        errorWidget: (context, url, error) => new Icon(Icons.error),
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({
    Key key,
    this.name,
    this.date,
    this.avatarUrl,
  }) : super(key: key);

  final String name;
  final String date;
  final String avatarUrl;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            child: CachedNetworkImage(
              imageUrl: avatarUrl,
              placeholder: (context, url) => new CircularProgressIndicator(),
              errorWidget: (context, url, error) => new Icon(Icons.error),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(name,
                    style: FribbbleThemeData.textTheme.body1
                ),
                Text(date,
                    style: FribbbleThemeData.textTheme.body2),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
