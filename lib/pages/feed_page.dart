import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fribbble/bloc/feed_bloc.dart';
import 'package:fribbble/bloc_provider.dart';
import 'package:fribbble/gateway/feed_gateway.dart';
import 'package:fribbble/pages/feed/post_card.dart';
import 'package:fribbble/pages/project_add/title_page.dart';
import 'package:fribbble/views/feed_post_view.dart';

class FeedPage extends StatelessWidget {
  static const String tag = 'feed-page';

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      bloc: FeedBloc(FeedGateway(Firestore())),
      child: _FeedPage(),
    );
  }
}

class _FeedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<FeedBloc>(context);
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
          statusBarIconBrightness: Brightness.dark,
          statusBarColor: Color(0x11000000),
        ),
        child: SafeArea(
          bottom: false,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.fromLTRB(24, 16, 24, 0),
                  child: Text(
                    "Fribbble - Feed",
                    style: Theme.of(context).textTheme.display1,
                  ),
                ),
                StreamBuilder<List<FeedPostView>>(
                  stream: bloc.posts,
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return const Center(child: CircularProgressIndicator());
                    }
                    final List<FeedPostView> feedList = snapshot.data;
                    return Column(
                      children: <Widget>[
                        Column(
                          children: feedList
                              .map((feedObject) => PostCard(
                                    post: feedObject,
                                  ))
                              .toList(),
                        ),
                      ],
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => ProjectAddTitlePage())),
      ),
    );
  }
}
