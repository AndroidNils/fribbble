import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fribbble/models/Store.dart';
import 'package:fribbble/views/feed_post_view.dart';
import 'package:url_launcher/url_launcher.dart';

import '../FribbleTheme.dart';

class PostDetailPage extends StatelessWidget {
  static const String tag = 'post-detail-page';
  final FeedPostView post;

  const PostDetailPage(this.post) : super();

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: LayoutBuilder(
      builder: (BuildContext context, BoxConstraints viewportConstraints) {
        return AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle(
            statusBarColor: Color(0x22000000),
          ),
          child: SafeArea(
            bottom: false,
            child: SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: viewportConstraints.maxHeight,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _Header(
                      date: post.createdOnDate,
                      name: post.creatorName,
                      avatarUrl: post.avatarPictureUrl,
                    ),
                    Image.network(post.files[0]),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16, 8, 16, 0),
                      child: Text(
                        post.title,
                        style: Theme.of(context).textTheme.display1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16, 8, 16, 0),
                      child: Text(
                        post.description,
                        style: Theme.of(context).textTheme.body1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Icon(
                              Icons.star,
                              color: Color(0xFFF7C203),
                            ), // TODO: Change based on User in post.likedBy
                            Text(post.stars.toString(),
                                style: Theme.of(context)
                                    .textTheme
                                    .body2
                                    .copyWith(color: Color(0xFFF7C203)))
                          ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(24),
                      child: Text(
                        'GET IT',
                        style: FribbbleThemeData.textTheme.subtitle,
                      ),
                    ),
                    Row(
                      children: _getBadges(context),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    ));
  }

  List<Widget> _getBadges(BuildContext context) {
    List<Widget> badges = List<Widget>();
    if (post.stores != null) {
      post.stores.forEach((store, url) => badges.add(GestureDetector(
            onTap: _launchURL(url),
            child: Container(
              child: SvgPicture.asset(_badgePath(store)),
            ),
          )));
    }
    if (badges.isEmpty)
      badges.add(Container(
        width: 0,
        height: 0,
      ));
    return badges;
  }

  String _badgePath(String store) {
    switch (store) {
      case Store.google:
        return 'assets/get_it_on_play.svg';
      case Store.iOS:
        return 'assets/get_it_on_app_store.svg';
      case Store.web:
        return 'assets/get_it_on_web.svg';
    }
    return '';
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class _Header extends StatelessWidget {
  const _Header({
    Key key,
    this.name,
    this.date,
    this.avatarUrl,
  }) : super(key: key);

  final String name;
  final String date;
  final String avatarUrl;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: <Widget>[
          CircleAvatar(
            child: CachedNetworkImage(
              imageUrl: avatarUrl,
              placeholder: (context, url) => new CircularProgressIndicator(),
              errorWidget: (context, url, error) => new Icon(Icons.error),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(name, style: FribbbleThemeData.textTheme.body1),
                Text(date, style: FribbbleThemeData.textTheme.body2),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
