import 'package:fribbble/bloc_provider.dart';
import 'package:fribbble/gateway/feed_gateway.dart';
import 'package:fribbble/models/post.dart';
import 'package:fribbble/views/feed_post_view.dart';
import 'package:intl/intl.dart';
import 'package:rxdart/subjects.dart';

class FeedBloc extends BlocBase {
  final FeedGateway _gateway;
  FeedBloc(this._gateway) {
    addPostsToStream();
  }

  Future addPostsToStream() async {
    final List<DatabasePost> dbPosts = await _gateway.loadPosts();
    final List<Post> posts = dbPosts.map(dbPostToPost).toList();
    final List<FeedPostView> views = posts.map(postToView).toList();
    _postsSubject.add(views);
  }

  Stream<List<FeedPostView>> get posts => _postsSubject;

  BehaviorSubject<List<FeedPostView>> _postsSubject =
      BehaviorSubject<List<FeedPostView>>();

  @override
  void dispose() {
    _postsSubject.close();
  }

  Post dbPostToPost(DatabasePost dbPost) {
    final creator = User(dbPost.creatorName, _avatarUrl);
    return Post(
        title: dbPost.title,
        description: dbPost.description,
        createdOn: dbPost.date.toDate(),
        creator: creator,
        pictureUrls: dbPost.files,
        stars: dbPost.likes,
        category: Category.school,
        starredByUser: false);
  }

  FeedPostView postToView(Post post) {
    return FeedPostView(
        title: post.title,
        avatarPictureUrl: post.creator.pictureUrl,
        createdOnDate: _dateFormat.format(post.createdOn),
        creatorName: post.creator.name,
        files: post.pictureUrls,
        description: post.description,
        starredByUser: post.starredByUser,
        stars: post.stars);
  }

  final _dateFormat = DateFormat('yyyy-MM-dd - kk:mm');
}

final _avatarUrl =
    "https://store.playstation.com/store/api/chihiro/00_09_000/container/DE/de/999/EP0149-CUSA09988_00-AV00000000000002/1553528383000/image?w=240&h=240&bg_color=000000&opacity=100&_version=00_09_000";
