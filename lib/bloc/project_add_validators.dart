import 'dart:async';

class Validators {
  final validateTitle =
  StreamTransformer<String, String>.fromHandlers(handleData: (title, sink) {
    if (NotEmptyOrNullValidator(title).isValid()) {
      sink.add(title);
    } else {
      sink.addError(TextValidationException(emptyTitleUserMessage));
    }
  });
  
  static const emptyTitleUserMessage = "Bitte gib einen Titel für den Eintrag an!";
  static const emptyCourseUserMessage = "Bitte gib eine Beschreibung für den Eintrag an!";
}

class TextValidationException implements Exception{
  final String message;

  TextValidationException([this.message]);

  @override
  String toString() => message ?? "TextValidationException";
}

class NotEmptyOrNullValidator implements Validator {
  final String courseName;

  NotEmptyOrNullValidator(this.courseName);

  @override
  bool isValid() {
    if (courseName != null && courseName.isNotEmpty) return true;
    return false;
  }
}

abstract class Validator {
  bool isValid();
}