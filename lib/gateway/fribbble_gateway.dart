import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';

class FribbbleGateway {
  final FirebaseUser firebaseUser;
  final String uID;

  static Firestore fStore = Firestore.instance;
  static FirebaseAuth auth = FirebaseAuth.instance;
  static FirebaseStorage fStorage = FirebaseStorage.instance;

  FribbbleGateway(this.firebaseUser) : uID = firebaseUser.uid;

  Stream<FirebaseUser> get firebaseUserStream => auth.onAuthStateChanged;
}