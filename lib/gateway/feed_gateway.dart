import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:meta/meta.dart';

class FeedGateway {
  final Firestore _firestore;

  FeedGateway(this._firestore);

  Future<List<DatabasePost>> loadPosts() async {
    final collectionQuery = await _firestore.collection("posts").getDocuments();
    final dbPosts =  collectionQuery.documents.map((doc) => DatabasePost.fromData(doc)).toList();
    return dbPosts;
  }
}

class DatabasePost {
  final String id;
  final String category;
  final String creatorID;
  final String creatorName;
  final Timestamp date;
  final String description;
  final List<String> likedBy;
  final int likes;
  final String title;
  final Map<String, String> stores;
  final List<String> files;

  DatabasePost._({
    @required this.id,
    @required this.category,
    @required this.creatorID,
    @required this.creatorName,
    @required this.date,
    @required this.description,
    @required this.likedBy,
    @required this.title,
    @required this.stores,
    @required this.files,
    @required this.likes,
  });

  factory DatabasePost.create() {
    return DatabasePost._(
        id: '',
        category: '',
        creatorID: '',
        creatorName: '',
        date: Timestamp.now(),
        description: '',
        likedBy: List(),
        title: '',
        stores: Map(),
        files: List(),
        likes: 0);
  }

  factory DatabasePost.fromData(dynamic data) {
    return DatabasePost._(
      id: data.documentID,
      category: data['category'],
      creatorID: data['creatorID'],
      creatorName: data['creatorName'],
      date: data['date'],
      description: data['description'],
      likedBy: List<String>.from((data['likedBy'] as List<dynamic>).where((element) => element is String).toList()),
      title: data['title'],
      stores: Map.castFrom<dynamic, dynamic, String, String>(data['stores']),
      files:  List<String>.from((data['files'] as List<dynamic>).where((element) => element is String).toList()),
      likes: data["likes"]
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'category': category,
      'creatorID': creatorID,
      'creatorName': creatorName,
      'date': date,
      'description': description,
      'likedBy': likedBy,
      'title': title,
      'stores': stores,
      'files': files,
      'likes': likes
    };
  }

  DatabasePost copyWith(
      {String id,
      String category,
      String creatorID,
      String creatorName,
      String date,
      String description,
      List<String> likedBy,
      String title,
      Map<String, String> stores,
      Map<String, String> files}) {
    return DatabasePost._(
        id: id ?? this.id,
        category: category ?? this.category,
        creatorID: creatorID ?? this.creatorID,
        creatorName: creatorName ?? this.creatorName,
        date: date ?? this.date,
        description: description ?? this.description,
        likedBy: likedBy ?? this.likedBy,
        title: title ?? this.title,
        stores: stores ?? this.stores,
        files: files ?? this.files,
        likes: likes ?? this.likes);
  }
}
