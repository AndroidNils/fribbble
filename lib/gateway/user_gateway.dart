import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fribbble/gateway/fribbble_gateway.dart';
import 'package:fribbble/models/user.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/subjects.dart';

class UserGateway {
  final CollectionReference userCollection = FribbbleGateway.fStore.collection('user');
  final FirebaseUser firebaseUser;
  final String uID;
  final StreamController<User> streamOfParsedUser = BehaviorSubject<User>();

  Stream<User> get userStream => _userStream.stream;
  final BehaviorSubject<User> _userStream = BehaviorSubject<User>();
  StreamTransformer<DocumentSnapshot, User> _userTransformer;

  UserGateway(this.firebaseUser) : uID = firebaseUser.uid {
    initializeUserTransformerAndUserStream();
  }

  void initializeUserTransformerAndUserStream() {
    _userTransformer = StreamTransformer<DocumentSnapshot, User>.fromHandlers(
        handleData: (userDoc, sink) {
      try {
        User user = User.fromData(userDoc.data);
        sink.add(user);
      } on Exception catch (e, s) {
        sink.addError(e, s);
      }
    });
    _userStream.addStream(userCollection
        .document(firebaseUser.uid)
        .snapshots()
        .transform(_userTransformer));
  }

  Future<void> logOut() async {
    await FribbbleGateway.auth.signOut();
  }

  Future<void> setUsername(String name) async {
    UserUpdateInfo userInfo = UserUpdateInfo();
    userInfo.displayName = name;
    await firebaseUser.updateProfile(userInfo);
    print("Changed FirebaseUser.displayName to: $name");
  }

  Future<void> changeEmail(String email) async {
    await firebaseUser.updateEmail(email);
    return;
  }

  Future<void> addUser({@required User user, bool merge = false}) async {
    assert(user != null);
    await userCollection
        .document(firebaseUser.uid)
        .setData(user.toJson(), merge: merge);

    return;
  }

  Future<bool> deleteUser(FribbbleGateway gateway) async {
    final currentUser = await FribbbleGateway.auth.currentUser();
    return currentUser.delete().then((_) => true);
  }

  Future<void> createUser(String pName,
      [String affiliatedByCode]) async {
    try {
      User user = User.create().copyWith(
        id: uID,
        name: pName,
      );

      print(user);
      await userCollection.document(uID).setData(user.toJson());
    } catch (e, s) {
      print(e);
      print(s);
      rethrow;
    }
  }

  void dispose() {
    _userStream.close();
    streamOfParsedUser.close();
  }
}