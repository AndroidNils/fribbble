import 'package:fribbble/bloc_provider.dart';
import 'package:fribbble/gateway/fribbble_gateway.dart';

class FribbbleContext extends BlocBase {
    final FribbbleGateway gateway;

    FribbbleContext(this.gateway);
    
    @override
    void dispose() {
    }
}