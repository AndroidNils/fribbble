class Post {
  final Category category;
  final String title;
  final String description;
  final DateTime createdOn;
  final User creator;
  final List<String> pictureUrls;
  final int stars;
  final bool starredByUser;
  bool get createdByLoggedInUser => true;

  Post({this.title, this.description, this.createdOn, this.creator,
      this.pictureUrls, this.stars, this.category, this.starredByUser});
}

class User {
  final String name;
  final String pictureUrl;

  User(this.name, this.pictureUrl);
}

enum Category { books, school }
