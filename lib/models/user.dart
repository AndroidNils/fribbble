import 'package:meta/meta.dart';


class User {
  final String id;
  final String name;
  final String pictureUrl;
  final String description;

  User._({
    @required this.id,
    @required this.name,
    @required this.pictureUrl,
    @required this.description,
  });

  factory User.create() {
    return User._(
        id: "",
        name: "",
        pictureUrl: "",
        description: "", 
    );
  }

factory User.fromData(dynamic data) {
    return User._(
      id: data.documentID, 
      name: data['name'], 
      pictureUrl: data['pictureUrl'],
      description: data['description'],
      );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'pictureUrl': pictureUrl,
      'description': description,
    };
  }

  User copyWith({
    String id, String name, String abbreviation,
  }){
    return User._(
        id: id ?? this.id,
        name: name ?? this.name,
        pictureUrl: pictureUrl ?? this.pictureUrl,
        description: description ?? this.description,
    );
  }

}
